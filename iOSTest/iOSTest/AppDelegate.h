//
//  AppDelegate.h
//  iOSTest
//
//  Created by Jenkins on 9/6/16.
//  Copyright © 2016 Jenkins. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

